from tkinter import scrolledtext
import tkinter as tk
from DroneGUI.ActionListeners import ActionListeners

# BUTTON SIZE
MAXIMUM_WIDTH_UNIT_B = 30
BUTTON_HEIGHT_UNIT_B = 1
# TEXTAREA SIZE
MAXIMUM_WIDTH_UNIT_TEXT = 100
MAXIMUM_HEIGHT_UNIT_TEXT = 20


class Application(tk.Frame):
    def __init__(self, master=None):
        self.emotiv = None
        self.drone = None
        # SCREEN ELEMENTS
        self.arrayOfCommandButtons = []
        self.frame = tk.Frame.__init__(self, master)
        self.textArea = scrolledtext.ScrolledText(self.master, height=MAXIMUM_HEIGHT_UNIT_TEXT,
                                                  width=MAXIMUM_WIDTH_UNIT_TEXT)
        self.textArea.bind("<Key>", lambda e: "break")
        self.actionListeners = ActionListeners(self.textArea, self.emotiv, self.drone)
        self.grid()
        self.createWidgets()
        self.actionListeners.setDroneBattery(self.dronebattery)


    def addButtonToGrid(self):
        # ADD BUTTONS TO DISPLAY GRID
        self.startEmotivButton.grid(row=1, column=0, sticky="nsew")
        self.commandForward.grid(row=2, column=0, sticky="nsew")
        self.commandRight.grid(row=3, column=0, sticky="nsew")
        self.commandLeft.grid(row=4, column=0, sticky="nsew")
        self.commandBackward.grid(row=5, column=0, sticky="nsew")
        self.commandUp.grid(row=6, column=0, sticky="nsew")
        self.commandDown.grid(row=7, column=0, sticky="nsew")
        self.commandSavedPreset.grid(row=8, column=0, sticky="nsew")
        self.commandLoadPreviousPreset.grid(row=9, column=0, sticky="nsew")
        self.commandSaveCurrentPreset.grid(row=10, column=0, sticky="nsew")
        self.commandStartMentalComparison.grid(row=11, column=0, sticky="nsew")
        self.commandStopMentalComparison.grid(row=12, column=0, sticky="nsew")
        self.mirrorSwitch.grid(row=13, column=0, sticky="nsew")
        self.startDrone.grid(row=14, column=0, sticky="nsew")
        self.landDrone.grid(row=15, column=0, sticky="nsew")
        self.takeOffDrone.grid(row=16, column=0, sticky="nsew")
        self.controlDroneByKey.grid(row=17, column=0, sticky="nsew")
        self.droneVirtual.grid(row=18, column=0, sticky="nsew")
        self.startRecord.grid(row=19, column=0, sticky="nsew")
        self.replayRecord.grid(row=20, column=0, sticky="nsew")
        self.dronebattery.grid(row=21, column=0, sticky="nsew")

    def createButtons(self):
        # CREATE BUTTON
        self.startEmotivButton = tk.Button(self, text='(Re)Start Headset', height=BUTTON_HEIGHT_UNIT_B, width=MAXIMUM_WIDTH_UNIT_B, compound=tk.CENTER, command = self.actionListeners.startEmotiv)

        #TRAINING BUTTONS - THREADING
        self.commandForward = tk.Button(self, text='Train Forward', height=BUTTON_HEIGHT_UNIT_B, width=MAXIMUM_WIDTH_UNIT_B, command=lambda : self.actionListeners.setCommandForward(self.arrayOfCommandButtons))
        self.commandRight = tk.Button(self, text='Train Right', height=BUTTON_HEIGHT_UNIT_B, width=MAXIMUM_WIDTH_UNIT_B, command=lambda : self.actionListeners.setCommandRight(self.arrayOfCommandButtons))
        self.commandLeft = tk.Button(self, text='Train Left', height=BUTTON_HEIGHT_UNIT_B, width=MAXIMUM_WIDTH_UNIT_B, command=lambda : self.actionListeners.setCommandLeft(self.arrayOfCommandButtons))
        self.commandBackward = tk.Button(self, text='Train Backward', height=BUTTON_HEIGHT_UNIT_B, width=MAXIMUM_WIDTH_UNIT_B, command=lambda : self.actionListeners.setCommandBackward(self.arrayOfCommandButtons))
        self.commandUp = tk.Button(self, text='Train Up', height=BUTTON_HEIGHT_UNIT_B, width=MAXIMUM_WIDTH_UNIT_B, command=lambda : self.actionListeners.setCommandUp(self.arrayOfCommandButtons))
        self.commandDown = tk.Button(self, text='Train Down', height=BUTTON_HEIGHT_UNIT_B, width=MAXIMUM_WIDTH_UNIT_B, command=lambda : self.actionListeners.setCommandDown(self.arrayOfCommandButtons))
        self.arrayOfCommandButtons.append(self.commandForward)
        self.arrayOfCommandButtons.append(self.commandRight)
        self.arrayOfCommandButtons.append(self.commandLeft)
        self.arrayOfCommandButtons.append(self.commandBackward)
        self.arrayOfCommandButtons.append(self.commandUp)
        self.arrayOfCommandButtons.append(self.commandDown)

        # None Threading
        self.commandSavedPreset = tk.Button(self, text="Load Hardcoded Preset", height=BUTTON_HEIGHT_UNIT_B, width=MAXIMUM_WIDTH_UNIT_B, compound=tk.CENTER, command= self.actionListeners.savedPreset)
        self.commandSaveCurrentPreset = tk.Button(self, text="Save Preset", height=BUTTON_HEIGHT_UNIT_B, width=MAXIMUM_WIDTH_UNIT_B, compound=tk.CENTER, command= self.actionListeners.SaveCurrentPreset)
        self.commandLoadPreviousPreset = tk.Button(self, text="Load Preset", height=BUTTON_HEIGHT_UNIT_B, width=MAXIMUM_WIDTH_UNIT_B, compound=tk.CENTER, command= self.actionListeners.LoadPreviousPreset)

        # THREADING
        self.commandStartMentalComparison = tk.Button(self, text="Start Mental Commands", height=BUTTON_HEIGHT_UNIT_B, width = MAXIMUM_WIDTH_UNIT_B, command=lambda : self.actionListeners.startMentalCommands(self))

        # None Threading
        self.commandStopMentalComparison = tk.Button(self, text="Stop Mental Commands", height=BUTTON_HEIGHT_UNIT_B, width=MAXIMUM_WIDTH_UNIT_B, command=self.actionListeners.stopMentalCommands) #
        self.mirrorSwitch = tk.Button(self, text="mirror Switch Commands", height=BUTTON_HEIGHT_UNIT_B, width=MAXIMUM_WIDTH_UNIT_B, command=self.actionListeners.mirrorSwitchCommands) #
        self.landDrone = tk.Button(self, text="Land Drone", height=BUTTON_HEIGHT_UNIT_B, width=MAXIMUM_WIDTH_UNIT_B, command=self.actionListeners.land)
        self.takeOffDrone = tk.Button(self, text="Takeoff Drone", height=BUTTON_HEIGHT_UNIT_B, width=MAXIMUM_WIDTH_UNIT_B, command= self.actionListeners.takeOff)

        # THREADING
        self.startDrone = tk.Button(self, text="Start Drone", height=BUTTON_HEIGHT_UNIT_B, width=MAXIMUM_WIDTH_UNIT_B, command=lambda : self.actionListeners.startDrone(self)) #
        self.controlDroneByKey = tk.Button(self, text="Control by Keyboard", height=BUTTON_HEIGHT_UNIT_B, width=MAXIMUM_WIDTH_UNIT_B, command=lambda : self.actionListeners.controlDroneByKey(self)) #
        self.droneVirtual = tk.Button(self, text="Virtual Drone", height=BUTTON_HEIGHT_UNIT_B, width=MAXIMUM_WIDTH_UNIT_B, command=lambda : self.actionListeners.startVirtualDrone(self))
        self.startRecord = tk.Button(self, text="Record Movement", height=BUTTON_HEIGHT_UNIT_B, width=MAXIMUM_WIDTH_UNIT_B, command=lambda : self.actionListeners.recordEmotivData(self))
        self.replayRecord = tk.Button(self, text="Replay Record", height=BUTTON_HEIGHT_UNIT_B, width=MAXIMUM_WIDTH_UNIT_B, command=lambda : self.actionListeners.replayRecording(self))

        #Button for drone battery
        self.dronebattery = tk.Button(self, text="Drone Battery: ", state="disable", height=BUTTON_HEIGHT_UNIT_B, width=MAXIMUM_WIDTH_UNIT_B)


    def createWidgets(self):
        self.createButtons()
        self.addButtonToGrid()
        self.textArea.grid(row=0, column=1, sticky="nsew")
        self.textArea.insert(tk.END, "")


"""# FRAME
ws = tk.Tk()
ws.title('PythonGuides')
ws.geometry(str(WIDTH) + 'x' + str(HEIGHT))
# APPLICATION
app = Application(ws)
app.master.title('Sample Action')
app.mainloop()"""
