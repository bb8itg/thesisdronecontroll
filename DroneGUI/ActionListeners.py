from EmotivInterfaceHandle.EmotivSignalForButtons import EmotivSingalForButtons
from DroneControl.MovementEnum import movement
from DroneControl.droneControl import droneControl
import numpy as np
from time import sleep
from matplotlib.figure import Figure
import matplotlib.animation as animation
from matplotlib.backends.backend_tkagg import (FigureCanvasTkAgg,
                                               NavigationToolbar2Tk)
from tkinter import filedialog
import math
from threading import Thread
import tkinter as tk


class ActionListeners:
    def __init__(self, textArea, emotiv, drone):
        self.textArea = textArea
        self.emotiv = emotiv
        self.isMentalCommandOn = [True]
        self.isPopupActive = False
        self.dronebattery = None

        # COMMUNICATE WITH DRONE
        self.drone = drone
        self.isDroneCreated = False
        self.control = [movement.STAY]

    # ======= DRONE ACTIONLISTENERS ========
    def startDrone(self, app):
        if not self.isDroneCreated:
            self.drone = droneControl(self.control, self.textArea)
            self.drone.setBatteryButton(self.dronebattery)
            self.isDroneCreated = True
        #        Thread(target=self.drone.getDroneVision).start()
        Thread(target=self.drone.movementStart, args=(app.startDrone,), daemon=True).start()

    def land(self):
        try:
            self.drone.land()
        except AttributeError:
            self.textArea.insert(tk.END, "Start Drone\n")
            self.textArea.see(tk.END)

    def takeOff(self):
        try:
            self.drone.takeOff()
        except AttributeError:
            self.textArea.insert(tk.END, "Start Drone\n")
            self.textArea.see(tk.END)

    def controlDroneByKey(self, app):
        try:
            self.isMentalCommandOn[0] = False
            Thread(target=self.drone.controlDroneByKey, args=(app.controlDroneByKey,), daemon=True).start()
        except AttributeError:
            self.textArea.insert(tk.END, "Start Drone\n")
            self.textArea.see(tk.END)

    # ======= EMOTIV ACTIONLISTENERS =======
    def SaveCurrentPreset(self):
        try:
            filename = filedialog.asksaveasfilename(initialdir="/", title="Select a File",
                                                    filetypes=(("JSON files", "*.json*"), ("all files", "*.*")))
            self.emotiv.saveCustomPreset(filename)
        except AttributeError:
            self.textArea.insert(tk.END, "Start Headset\n")
            self.textArea.see(tk.END)

    def LoadPreviousPreset(self):
        try:
            filename = filedialog.askopenfilename(initialdir="/", title="Select a File",
                                                  filetypes=(("JSON files", "*.json*"), ("all files", "*.*")))
            self.emotiv.loadCustomPreset(filename)
        except AttributeError:
            self.textArea.insert(tk.END, "Start Headset\n")
            self.textArea.see(tk.END)

    def setCommandForward(self, arrayOfCommandButtons):
        try:
            Thread(target=self.emotiv.setCommandValue,
                   args=(self.emotiv.forward, self.emotiv.forwardMot, arrayOfCommandButtons), daemon=True).start()
            self.textArea.insert(tk.END, "Set Command: Forward\n")
            self.textArea.see(tk.END)
        except AttributeError:
            self.textArea.insert(tk.END, "Start Headset\n")
            self.textArea.see(tk.END)

    def setCommandRight(self, arrayOfCommandButtons):
        try:
            Thread(target=self.emotiv.setCommandValue,
                   args=(self.emotiv.right, self.emotiv.rightMot, arrayOfCommandButtons), daemon=True).start()
            self.textArea.insert(tk.END, "Set Command: Right\n")
            self.textArea.see(tk.END)
        except AttributeError:
            self.textArea.insert(tk.END, "Start Headset\n")
            self.textArea.see(tk.END)

    def setCommandLeft(self, arrayOfCommandButtons):
        try:
            Thread(target=self.emotiv.setCommandValue,
                   args=(self.emotiv.left, self.emotiv.leftMot, arrayOfCommandButtons), daemon=True).start()
            self.textArea.insert(tk.END, "Set Command: Left \n")
            self.textArea.see(tk.END)
        except AttributeError:
            self.textArea.insert(tk.END, "Start Headset\n")
            self.textArea.see(tk.END)

    def setCommandBackward(self, arrayOfCommandButtons):
        try:
            Thread(target=self.emotiv.setCommandValue,
                   args=(self.emotiv.backward, self.emotiv.backwardMot, arrayOfCommandButtons), daemon=True).start()
            self.textArea.insert(tk.END, "Set Command: Backward\n")
            self.textArea.see(tk.END)
        except AttributeError:
            self.textArea.insert(tk.END, "Start Headset\n")
            self.textArea.see(tk.END)

    def setCommandUp(self, arrayOfCommandButtons):
        try:
            Thread(target=self.emotiv.setCommandValue, args=(self.emotiv.up, self.emotiv.upMot, arrayOfCommandButtons),
                   daemon=True).start()
            self.textArea.insert(tk.END, "Set Command: Up\n")
            self.textArea.see(tk.END)
        except AttributeError:
            self.textArea.insert(tk.END, "Start Headset\n")
            self.textArea.see(tk.END)

    def setCommandDown(self, arrayOfCommandButtons):
        try:
            Thread(target=self.emotiv.setCommandValue,
                   args=(self.emotiv.down, self.emotiv.downMot, arrayOfCommandButtons), daemon=True).start()
            self.textArea.insert(tk.END, "Set Command: Down\n")
            self.textArea.see(tk.END)
        except AttributeError:
            self.textArea.insert(tk.END, "Start Headset\n")
            self.textArea.see(tk.END)

    def startMentalCommands(self, app):
        try:
            self.isMentalCommandOn[0] = True
            Thread(target=self.emotiv.startHeadSet, args=(app.commandStartMentalComparison,), daemon=True).start()
            self.textArea.insert(tk.END, "Mental Commands Started\n")
            self.textArea.see(tk.END)

        except AttributeError:
            self.textArea.insert(tk.END, "Start Headset\n")
            self.textArea.see(tk.END)

    def stopMentalCommands(self):
        self.isMentalCommandOn[0] = False

    def savedPreset(self):
        try:
            self.emotiv.hardcodedCommandValues()
        except AttributeError:
            self.textArea.insert(tk.END, "Start Headset\n")
            self.textArea.see(tk.END)

    def mirrorSwitchCommands(self):
        try:
            self.emotiv.mirrorCommandsToSwitch()
        except AttributeError:
            self.textArea.insert(tk.END, "Start Headset\n")
            self.textArea.see(tk.END)

    def startEmotiv(self):
        try:
            self.emotiv = EmotivSingalForButtons(self.textArea, self.isMentalCommandOn, self.control)
            self.emotiv.setUpEmotiv()
            self.textArea.insert(tk.END, "Headset is connected, now you can connect to the drone\n")
            self.textArea.see(tk.END)
            self.emotiv.setSwitches()
            if not self.isPopupActive:
                Thread(target=self.popup_window, daemon=True).start()
                self.isPopupActive = True
        except KeyError:
            self.textArea.insert(tk.END, "Turn on your headset\n")
            self.textArea.see(tk.END)
        except IndexError:
            self.textArea.insert(tk.END, "Turn on your headset\n")
            self.textArea.see(tk.END)

    def startVirtualDrone(self, app):
        Thread(target=self.popup_for_virtualDrone, args=(app.droneVirtual,), daemon=True).start()

    def recordEmotivData(self, app):
        try:
            filename = filedialog.asksaveasfilename(initialdir="/", title="Select a File",
                                                    filetypes=(("JSON files", "*.json*"), ("all files", "*.*")))
            Thread(target=self.emotiv.saveRecording, args=(filename, app.startRecord), daemon=True).start()
        except AttributeError:
            self.textArea.insert(tk.END, "Start Headset\n")
            self.textArea.see(tk.END)

    def replayRecording(self, app):
        try:
            index = [0]
            isReplayOn = [False]
            loadedRecords = []
            filename = filedialog.askopenfilename(initialdir="/", title="Select a File",
                                                  filetypes=(("JSON files", "*.json*"), ("all files", "*.*")))
            Thread(target=self.emotiv.replayRecord, args=(filename, app.replayRecord, index, loadedRecords, isReplayOn),
                   daemon=True).start()
            Thread(target=self.replayRecordpopUp, args=(index, app.replayRecord, loadedRecords, isReplayOn),
                   daemon=True).start()
        except AttributeError:
            self.textArea.insert(tk.END, "Start Headset\n")
            self.textArea.see(tk.END)

    # ================== POP UP WINDOW ======================
    def replayRecordpopUp(self, index, button, loadedRecords, isReplayOn):
        replayWindow = tk.Toplevel()
        self.creatElementsForReplayPopUp(replayWindow, button, index, loadedRecords, isReplayOn)

    def creatElementsForReplayPopUp(self, window, button, index, loadedRecords, isReplayOn):
        play = tk.Button(window, text="PLAY", command=lambda: self.resumeReplay(isReplayOn))
        stop = tk.Button(window, text="PAUSE", command=lambda: self.pauseReplay(isReplayOn))
        slide = tk.Scale(window, from_=0, to=len(loadedRecords), orient=tk.HORIZONTAL, length=600)
        play.grid(row=0, column=0)
        stop.grid(row=0, column=1)
        slide.grid(row=0, column=2)

        Thread(target=self.slideValue, args=(slide, index, window, button), daemon=True).start()

    def pauseReplay(self, isReplayOn):
        isReplayOn[0] = False

    def resumeReplay(self, isReplayOn):
        isReplayOn[0] = True

    def slideValue(self, slide, index, window, button):
        try:
            while window.winfo_exists() and button["state"] == "disabled":
                slide["state"] = "active"
                slide.set(index[0])
                slide["state"] = "disable"
                sleep(0.5)
        except tk._tkinter.TclError:
            self.textArea.insert(tk.END, "Player for recording has been closed\n")
            self.textArea.see(tk.END)
        finally:
            window.destroy()

    def createPlotsForMatrix(self, window):
        fig0 = Figure(figsize=(5, 5), dpi=40)
        plot0 = fig0.add_subplot(111)
        canvasforward = FigureCanvasTkAgg(fig0, master=window)
        canvasforward.get_tk_widget().grid(row=0, column=0, sticky="nsew")

        fig1 = Figure(figsize=(5, 5), dpi=40)
        plot1 = fig1.add_subplot(111)
        canvasright = FigureCanvasTkAgg(fig1, master=window)
        canvasright.get_tk_widget().grid(row=1, column=0, sticky="nsew")

        fig2 = Figure(figsize=(5, 5), dpi=40)
        plot2 = fig2.add_subplot(111)
        canvasleft = FigureCanvasTkAgg(fig2, master=window)
        canvasleft.get_tk_widget().grid(row=2, column=0, sticky="nsew")

        fig3 = Figure(figsize=(5, 5), dpi=40)
        plot3 = fig3.add_subplot(111)
        canvasbackward = FigureCanvasTkAgg(fig3, master=window)
        canvasbackward.get_tk_widget().grid(row=0, column=1, sticky="nsew")

        fig4 = Figure(figsize=(5, 5), dpi=40)
        plot4 = fig4.add_subplot(111)
        canvasup = FigureCanvasTkAgg(fig4, master=window)
        canvasup.get_tk_widget().grid(row=1, column=1, sticky="nsew")

        fig5 = Figure(figsize=(5, 5), dpi=40)
        plot5 = fig5.add_subplot(111)
        canvasdown = FigureCanvasTkAgg(fig5, master=window)
        canvasdown.get_tk_widget().grid(row=2, column=1, sticky="nsew")

        fig6 = Figure(figsize=(5, 5), dpi=40)
        plot6 = fig6.add_subplot(111)
        canvascurrent = FigureCanvasTkAgg(fig6, master=window)
        canvascurrent.get_tk_widget().grid(row=0, column=2, sticky="nsew")

        while window.winfo_exists():
            plot0.clear()
            plot1.clear()
            plot2.clear()
            plot3.clear()
            plot4.clear()
            plot5.clear()
            plot6.clear()

            plot0.set_title("FORWARD", fontsize=20)
            plot1.set_title("RIGHT", fontsize=20)
            plot2.set_title("LEFT", fontsize=20)
            plot3.set_title("BACKWARD", fontsize=20)
            plot4.set_title("UP", fontsize=20)
            plot6.set_title("CURRENT", fontsize=20)
            plot5.set_title("DOWN", fontsize=20)

            plot0.plot(self.emotiv.forward)
            plot1.plot(self.emotiv.right)
            plot2.plot(self.emotiv.left)
            plot3.plot(self.emotiv.backward)
            plot4.plot(self.emotiv.up)
            plot5.plot(self.emotiv.down)
            plot6.plot(self.emotiv.data)
            canvascurrent.draw()

            canvasforward.draw()
            canvasright.draw()
            canvasleft.draw()
            canvasbackward.draw()
            canvasup.draw()
            canvasdown.draw()
            canvascurrent.draw()

            sleep(0.2)

    def createPlotForVirtualDrone(self, window):
        fig = Figure(figsize=(15, 15), dpi=40)
        ax = fig.add_subplot(111, projection='3d')
        canvas3Dvirtual = FigureCanvasTkAgg(fig, master=window)
        canvas3Dvirtual.get_tk_widget().grid(row=0, column=0, sticky="nsew")

        rotationFig = Figure(figsize=(15, 15), dpi=40)
        rotation = rotationFig.add_subplot(111)
        canvasRotation = FigureCanvasTkAgg(rotationFig, master=window)
        canvasRotation.get_tk_widget().grid(row=0, column=1, sticky="nsew")

        zxy = [0, 0, 0]
        # vector of angels able to represent 150 points
        angle = np.linspace(0, 2 * np.pi, 150)
        # represents the current index of angle
        rotateIndex = [0]
        # represent the incrementation of rotateIndex
        rotationSpeed = 3
        if rotationSpeed > len(angle):
            return -1

        radius = 5
        speed = 5
        while window.winfo_exists():
            self.controlDirectionConvertToXYZ(ax, zxy, speed, radius, angle, rotateIndex, rotationSpeed)
            self.controlTocanvasRotation(rotation, radius, angle, rotateIndex)
            # ax.scatter(zxy[0], zxy[1], zxy[2],s = 300, marker=">", c="red")
            canvasRotation.draw()
            canvas3Dvirtual.draw()
            sleep(0.2)

    def controlTocanvasRotation(self, rotation, radius, angle, rotateIndex):
        rotation.clear()
        rotation.set_title("Rotation Of Drone from Above", fontsize=20)
        x = radius * np.cos(angle[rotateIndex[0]])
        y = radius * np.sin(angle[rotateIndex[0]])
        rotation.plot(radius * np.cos(angle), radius * np.sin(angle))
        rotation.arrow(0, 0, x, y)
        rotation.scatter(0, 0, s=300, c="blue")
        rotation.scatter(x, y, s=1800, c="green")

    def controlDirectionConvertToXYZ(self, ax, zxy, speed, radius, angle, rotateIndex, rotationSpeed):
        rotatedZ = radius * np.cos(angle[rotateIndex[0]])
        rotatedX = radius * np.sin(angle[rotateIndex[0]])
        marker = ""

        if self.control[0] == movement.ROTATE_LEFT:
            marker = "<"
            rotateIndex[0] += rotationSpeed
            if rotateIndex[0] >= len(angle):
                rotateIndex[0] -= len(angle)
            ax.scatter(zxy[0] + rotatedZ, zxy[1] + rotatedX, zxy[2], s=300, marker=marker, c="green")

            return

        elif self.control[0] == movement.FORWARD:
            marker = "^"
            zxy[0] += rotatedZ
            zxy[1] += rotatedX

        elif self.control[0] == movement.ROTATE_RIGHT:
            marker = ">"
            rotateIndex[0] -= rotationSpeed
            if rotateIndex[0] < 0:
                rotateIndex[0] += len(angle)
            ax.scatter(zxy[0] + rotatedZ, zxy[1] + rotatedX, zxy[2], s=300, marker=marker, c="blue")

            return

        elif self.control[0] == movement.LEFT:
            pass

        elif self.control[0] == movement.STAY:
            pass

        elif self.control[0] == movement.RIGHT:
            pass

        elif self.control[0] == movement.UP:
            marker = "^"
            zxy[2] += speed

        elif self.control[0] == movement.BACK:
            marker = "v"
            zxy[0] -= rotatedZ
            zxy[1] -= rotatedX

        elif self.control[0] == movement.DOWN:
            marker = "v"
            zxy[2] -= speed

        ax.scatter(zxy[0], zxy[1], zxy[2], s=300, marker=marker, c="red")

    def popup_window(self):
        try:
            window = tk.Toplevel()
            self.createPlotsForMatrix(window)
        finally:
            self.isPopupActive = False

    def popup_for_virtualDrone(self, button):
        try:
            button['state'] = 'disabled'
            window = tk.Toplevel()
            self.createPlotForVirtualDrone(window)
        finally:
            button['state'] = 'active'

    def setDroneBattery(self, button):
        self.dronebattery = button
