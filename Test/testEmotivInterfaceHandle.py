import json
import unittest
from threading import Thread
from tkinter import scrolledtext
import tkinter as tk
from time import sleep
from DroneControl.MovementEnum import movement
from EmotivInterfaceHandle.EmotivSignalForButtons import EmotivSingalForButtons

SENSORNUMBERS = 5


class TestEmotivInterfaceHandle(unittest.TestCase):
    @classmethod
    def setUpClass(self):
        self.textArea = scrolledtext.ScrolledText(tk.Frame(), height=0, width=0)
        self.isMentalCommandOn = [True]
        self.control = [movement.STAY]
        self.emotiv = EmotivSingalForButtons(self.textArea, self.isMentalCommandOn, self.control)
        self.emotiv.setSwitches()

    def setUp(self):
        self.isMentalCommandOn = [True]
        self.emotiv.control = [movement.STAY]
        self.emotiv.forward = [[1000] * 5 for i in range(SENSORNUMBERS)]
        self.emotiv.backward = [[1000] * 5 for i in range(SENSORNUMBERS)]
        self.emotiv.right = [[1000] * 5 for i in range(SENSORNUMBERS)]
        self.emotiv.left = [[1000] * 5 for i in range(SENSORNUMBERS)]
        self.emotiv.up = [[1000] * 5 for i in range(SENSORNUMBERS)]
        self.emotiv.down = [[1000] * 5 for i in range(SENSORNUMBERS)]
        self.emotiv.dataFac = 'neutral'

    def test_compareInputWithCommands(self):
        self.emotiv.forward = [
            [4.97974, 6.70124, 3.26342, 5.5415, 3.6903]
            , [1.78114, 3.4149, 5.95846, 14.34814, 10.8744]
            , [5.31774, 9.17424, 1.82484, 2.17316, 1.54132]
            , [0.75424, 0.82396, 1.04422, 0.76748, 0.91452]
            , [6.11462, 7.57658, 3.45446, 6.55876, 4.09448]]
        self.emotiv.right = [
            [10.64808, 10.87766, 10.75988, 10.72396, 20.99724]
            , [30.5144, 30.37122, 30.70604, 30.17534, 21.43288]
            , [14.21588, 15.97484, 17.87366, 16.17892, 18.41662]
            , [9.06302, 9.05384, 9.91356, 14.36226, 11.75874]
            , [7.3032, 9.1657, 7.6972, 12.89378, 9.55336]
        ]
        self.emotiv.data = self.emotiv.forward

        self.emotiv.compareInputWithCommands()
        self.assertEqual(movement.FORWARD, self.emotiv.control[0], )

    def test_compareInputWithCommandsAndCompareRange(self):
        self.emotiv.forward = [
            [4.97974, 6.70124, 3.26342, 5.5415, 3.6903]
            , [1.78114, 3.4149, 5.95846, 14.34814, 10.8744]
            , [5.31774, 9.17424, 1.82484, 2.17316, 1.54132]
            , [0.75424, 0.82396, 1.04422, 0.76748, 0.91452]
            , [6.11462, 7.57658, 3.45446, 6.55876, 4.09448]]
        self.emotiv.right = [
            [4.98974, 6.80124, 3.36342, 5.6415, 3.7903]
            , [1.88114, 3.5149, 5.85846, 14.24814, 10.9744]
            , [5.21774, 9.27424, 1.92484, 2.27316, 1.64132]
            , [0.85424, 0.72396, 1.14422, 0.96748, 0.81452]
            , [10.21462, 10.67658, 10.55446, 10.65876, 10.29448]]  # this line is different
        self.emotiv.data = self.emotiv.right

        self.emotiv.compareInputWithCommands()
        self.assertEqual(movement.ROTATE_RIGHT, self.emotiv.control[0])

    def test_compareInputWithCommandsAndMaximumVictoryPoints(self):
        self.emotiv.forward = [
            [4.97974, 6.70124, 3.26342, 5.5415, 3.6903]
            , [1.78114, 3.4149, 5.95846, 14.34814, 10.8744]
            , [5.31774, 9.17424, 1.82484, 2.17316, 1.54132]
            , [0.75424, 0.82396, 1.04422, 0.76748, 0.91452]
            , [6.11462, 7.57658, 3.45446, 6.55876, 4.09448]]
        self.emotiv.right = [
            [0.97974, 0.70124, 0.26342, 0.5415, 0.6903]
            , [0.78114, 0.4149, 0.95846, 0.34814, 20.8744]
            , [20.31774, 20.17424, 20.82484, 20.17316, 20.54132]
            , [20.75424, 20.82396, 20.04422, 0.76748, 0.91452]
            , [6.11462, 7.57658, 3.45446, 6.55876, 4.09448]]
        self.emotiv.left = [
            [0.97974, 0.70124, 0.26342, 0.5415, 0.6903]
            , [0.78114, 0.4149, 0.95846, 0.34814, 0.8744]
            , [0.31774, 0.17424, 0.82484, 0.17316, 0.54132]
            , [0.75424, 0.82396, 0.04422, 20.76748, 20.91452]
            , [20.11462, 20.57658, 20.45446, 20.55876, 20.09448]]

        self.emotiv.data = [
            [4.97974, 6.70124, 3.26342, 5.5415, 3.6903]
            , [1.78114, 3.4149, 5.95846, 14.34814, 20.8744]
            , [20.31774, 20.17424, 20.82484, 20.17316, 20.54132]
            , [20.75424, 20.82396, 20.04422, 20.76748, 20.91452]
            , [20.11462, 20.57658, 20.45446, 20.55876, 20.09448]]

        self.emotiv.compareInputWithCommands()
        self.assertEqual(movement.STAY, self.emotiv.control[0])

    def test_compareInputWithCommandsAndMaximumDifferencePoints(self):
        self.emotiv.right = [
            [0.97974, 0.70124, 0.26342, 0.5415, 0.6903]
            , [0.78114, 0.4149, 0.95846, 0.34814, 20.8744]
            , [20.31774, 20.17424, 20.82484, 20.17316, 20.54132]
            , [20.75424, 20.82396, 20.04422, 0.76748, 0.91452]
            , [6.11462, 7.57658, 3.45446, 6.55876, 4.09448]]
        self.emotiv.left = [
            [0.97974, 0.70124, 0.26342, 0.5415, 0.6903]
            , [0.78114, 0.4149, 0.95846, 0.34814, 0.8744]
            , [0.31774, 0.17424, 0.82484, 0.17316, 0.54132]
            , [0.75424, 0.82396, 0.04422, 20.76748, 20.91452]
            , [20.11462, 20.57658, 20.45446, 20.55876, 20.09448]]

        self.emotiv.data = [
            [100.0, 100.0, 100.0, 100.0, 100.0]
            , [100.0, 100.0, 100.0, 100.0, 100.0]
            , [100.0, 100.0, 100.0, 100.0, 100.0]
            , [100.0, 100.0, 100.0, 100.0, 100.0]
            , [100.0, 100.0, 100.0, 100.0, 100.0]]

        self.emotiv.compareInputWithCommands()
        self.assertEqual(movement.STAY, self.emotiv.control[0])

    def test_replayRecord_(self):
        dummy = None
        isReplay = [True]
        Thread(target=self.setReplay, args=(isReplay,), daemon=True).start()
        self.emotiv.replayRecord('recording.JSON', dummy, [0], [], isReplay)
        testArray = [movement.FORWARD, movement.FORWARD, movement.ROTATE_LEFT, movement.ROTATE_RIGHT,
                     movement.ROTATE_RIGHT,
                     movement.ROTATE_LEFT, movement.FORWARD, movement.FORWARD, movement.ROTATE_RIGHT,
                     movement.ROTATE_RIGHT]
        self.assertEqual(testArray, self.emotiv.testArray)

    def setReplay(self, isReplay):
        sleep(5)
        self.emotiv.isMentalCommandOn[0] = False
        isReplay[0] = False

