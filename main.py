import tkinter as tk

from DroneGUI.Application import Application

WIDTH = 1050
HEIGHT = 550
# FRAME
ws = tk.Tk()
ws.title('PythonGuides')
ws.geometry(str(WIDTH) + 'x' + str(HEIGHT))
# APPLICATION
app = Application(ws)
app.master.title('Application')
app.mainloop()
