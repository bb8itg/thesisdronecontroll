from threading import Thread
import json
from time import sleep

from EmotivInterfaceHandle.cortex import Cortex

STREAMTYPE = 'pow'
STREAMTYPE2 = 'fac'
SAMPLENUMBER = 20
SINGALMISTAKES = 5
SENSORNUMBERS = 5
NUMBEROFCOMMANDS = 6
WAITINGTIME = 5
ROUNDING = 10
MAXIMUMDIFFERENCE = 5
DIFFERENCEEPSILON = 5



class EmotivSingal:
    def __init__(self):
        # COMMANDS
        self.isMotActive = False
        self.isFac = False

        # EEG POW
        self.forward = [[0] * 5 for i in range(SENSORNUMBERS)]
        self.backward = [[0] * 5 for i in range(SENSORNUMBERS)]
        self.right = [[0] * 5 for i in range(SENSORNUMBERS)]
        self.left = [[0] * 5 for i in range(SENSORNUMBERS)]
        self.up = [[0] * 5 for i in range(SENSORNUMBERS)]
        self.down = [[0] * 5 for i in range(SENSORNUMBERS)]

        # FACIAL
        if STREAMTYPE2 == 'fac':
            self.isFac = True

        self.switch1 = ["", self.forward, self.right, self.left]
        self.switch2 = ["", self.backward, self.up, self.down]

        # MOTION
        if STREAMTYPE2 == 'mot':
            self.isMotActive = True

        self.forwardMot = [0 for i in range(10)]
        self.backwardMot = [0 for i in range(10)]
        self.rightMot = [0 for i in range(10)]
        self.leftMot = [0 for i in range(10)]
        self.upMot = [0 for i in range(10)]
        self.downMot = [0 for i in range(10)]

        self.commandsArrayRef = [self.forward, self.backward, self.right, self.left]  # down up

        self.data = [[0] * 5 for i in range(SENSORNUMBERS)]
        self.dataMot = [0 for i in range(10)]
        self.dataFac = ""

        self.user = {
            "license": "",
            "client_id": "FNQTigpFd9ftHssVjFUcpfPhftV2zAk2S1WFCmEL",
            "client_secret": "P2S7QxmjK4Smiq3cHiHCh6koBO7I6B1aRk4S9PbUa4hFf4hUy9hSF3gi30RYvgzmePzbIluxQGJqqavz8nQsMBUnJW6gNbshBWyilkVhirL9MAf1NoKk3bAWqVpT3uUp",
            "debit": 100
        }
        self.c = Cortex(self.user, debug_mode=True)
        self.c.do_prepare_steps()

    def setSwitches(self):
        self.switch1[0] = 'neutral'
        self.switch2[0] = 'surprise'

    def hardcodedCommandValues(self):
        # SWITCH 1 IF USED
        self.forward = [
            [4.97974, 6.70124, 3.26342, 5.5415, 3.6903]
            ,[1.78114, 3.4149, 5.95846, 14.34814, 10.8744]
            ,[5.31774, 9.17424, 1.82484, 2.17316, 1.54132]
            ,[0.75424, 0.82396, 1.04422, 0.76748, 0.91452]
            ,[6.11462, 7.57658, 3.45446, 6.55876, 4.09448]]
        self.right = [
            [7.64808, 7.87766, 6.75988, 12.72396, 8.99724]
            ,[4.5144, 6.37122, 7.70604, 16.17534, 9.43288]
            ,[4.21588, 6.97484, 4.87366, 16.17892, 18.41662]
            ,[9.06302, 9.05384, 9.91356, 14.36226, 11.75874]
            ,[7.3032, 9.1657, 7.6972, 12.89378, 9.55336]
        ]
        self.left = [
            [7.14388, 4.79984, 2.07846, 0.63794, 0.87464]
            ,[8.94588, 3.74622, 2.14394, 1.6819, 1.9361]
            ,[3.91592, 3.21316, 1.06688, 0.50398, 0.67152]
            ,[2.01048, 2.4005, 1.50172, 0.94768, 0.80392]
            ,[9.74688, 4.97496, 1.73292, 0.50926, 0.75536]]

        # SWITCH 2 IF USED
        self.backward = [
            [4.97974, 6.70124, 3.26342, 5.5415, 3.6903]
            ,[1.78114, 3.4149, 5.95846, 14.34814, 10.8744]
            ,[5.31774, 9.17424, 1.82484, 2.17316, 1.54132]
            ,[0.75424, 0.82396, 1.04422, 0.76748, 0.91452]
            ,[6.11462, 7.57658, 3.45446, 6.55876, 4.09448]]
        self.up = [
            [7.64808, 7.87766, 6.75988, 12.72396, 8.99724]
            ,[4.5144, 6.37122, 7.70604, 16.17534, 9.43288]
            ,[4.21588, 6.97484, 4.87366, 16.17892, 18.41662]
            ,[9.06302, 9.05384, 9.91356, 14.36226, 11.75874]
            ,[7.3032, 9.1657, 7.6972, 12.89378, 9.55336]
        ]
        self.down = [
            [7.14388, 4.79984, 2.07846, 0.63794, 0.87464]
            ,[8.94588, 3.74622, 2.14394, 1.6819, 1.9361]
            ,[3.91592, 3.21316, 1.06688, 0.50398, 0.67152]
            ,[2.01048, 2.4005, 1.50172, 0.94768, 0.80392]
            ,[9.74688, 4.97496, 1.73292, 0.50926, 0.75536]]
        self.backward = self.forward
        self.up = self.right
        self.down = self.left


    def lastCheckBeforeRunOnCommands(self):
        if self.checkCommandUniqueness(self.forward, "forward") \
                and self.checkCommandUniqueness(self.backward, "backward") \
                and self.checkCommandUniqueness(self.right, "right") \
                and self.checkCommandUniqueness(self.left, "left"):
            print("Checking is done...")
        else:
            self.lastCheckBeforeRunOnCommands()

    #           and self.checkCommandUniqueness(self.up)
    #            andself.checkCommandUniqueness(self.down)

    def checkCommandUniqueness(self, array, commandname):
        counter = 0
        for temp in self.commandsArrayRef:
            if temp != array:
                counter = 0
                for i in range(len(self.forward)):
                    for j in range(len(self.forward)):
                        if array[i][j] - temp[i][j] < DIFFERENCEEPSILON:
                            counter += 1
            if counter >= MAXIMUMDIFFERENCE:
                print(commandname, " is to simmilar to one of the commands, registering new values to it...")
                self.timerBetweenCommandSetup()
                self.setCommandValue(array)
                return False
        return True

    def setUpEmotiv(self):
        stream = [STREAMTYPE, STREAMTYPE2]
        self.c.query_profile()
        self.c.setup_profile('amibira', 'load')
        Thread(target=self.c.sub_request, args=(stream,)).start()
        sleep(1)

    def showHeadsetSensorsWithText(self, data):
        print("    AF3: ", round(sum(data[0], 0)), "          AF4: ", round(sum(data[4]), 0))
        print("                          ")
        print("T7: ", round(sum(data[1], 0)), "                        T8: ", round(sum(data[3]), 0))
        print("                          ")
        print("            Pz: ", round(sum(data[2]), 0), "          ")

    # ====================== START SETTING UP API ========================
    def setCommands(self):
        stream = [STREAMTYPE, STREAMTYPE2]
        self.c.query_profile()
        self.c.setup_profile('amibira', 'load')
        Thread(target=self.c.sub_request, args=(stream,)).start()
        sleep(1)

        print("Setting the swicthes first")
        self.setSwitches()
        #switch1
        print("Forward")
        self.timerBetweenCommandSetup()
        self.setCommandValue(self.forward, self.forwardMot)

        print("Right")
        self.timerBetweenCommandSetup()
        self.setCommandValue(self.right, self.rightMot)

        print("Left")
        self.timerBetweenCommandSetup()
        self.setCommandValue(self.left, self.leftMot)
        #switch2
        print("Backward")
        self.timerBetweenCommandSetup()
        self.setCommandValue(self.backward, self.backwardMot)

        print("Up")
        self.timerBetweenCommandSetup()
        self.setCommandValue(self.up, self.upMot)

        print("Down")
        self.timerBetweenCommandSetup()
        self.setCommandValue(self.down, self.downMot)

#        self.lastCheckBeforeRunOnCommands()

        print("Forward")
        self.prettyPrintMatrix(self.forward)
        print("right")
        self.prettyPrintMatrix(self.right)
        print("left")
        self.prettyPrintMatrix(self.left)

        if STREAMTYPE2 == "mot":
            print("Forward")
            print(self.forwardMot)
            print("backward")
            print(self.backwardMot)
            print("right")
            print(self.rightMot)
            print("left")
            print(self.leftMot)

    #        print("up")
    #        self.prettyPrintMatrix(self.up)
    #        print("down")
    #        self.prettyPrintMatrix(self.down)

    def timerBetweenCommandSetup(self):
        input("Press any key to continue...")
        for i in range(WAITINGTIME):
            print("Next Command in : ", i, " sec")
            sleep(1)

    def setCommandValue(self, array, arrayMot):
        errorcount = 0
        overallMatrix = [[[0 for z in range(len(array))] for j in range(len(array))] for i in
                         range(SAMPLENUMBER)]


    # ================= EEG POWER DATA COLLECTION ===========================
        for time in range(0, SAMPLENUMBER):
            while True:
                rawData = json.loads(self.c.new_data)
                if STREAMTYPE in rawData:
                    rawData = rawData[STREAMTYPE]
                    break
            index = 0
            for i in range(0, len(self.data)):
                for j in range(0, len(self.data)):
                    self.data[i][j] = rawData[index]
                    index += 1

            for i in range(0, len(array)):
                for j in range(0, len(array)):
                    if (self.data[i][j]) < 100:
                        overallMatrix[time][i][j] = self.data[i][j]
                    else:
                        if time == 0:
                            overallMatrix[time][i][j] = 0
                        else:
                            overallMatrix[time][i][j] = overallMatrix[time - 1][i][j]
            print(time + 1, " out of", SAMPLENUMBER)
        for z in range(SAMPLENUMBER):
            for i in range(len(self.data)):
                for j in range(len(self.data)):
                    array[i][j] += round(overallMatrix[z][i][j], ROUNDING)

        for i in range(len(self.data)):
            for j in range(len(self.data)):
                array[i][j] = round(array[i][j] / SAMPLENUMBER, ROUNDING)
        if self.isMotActive:
            # ================= MOTION DATA COLLECTION ===========================
            print("Collecting motion data....")
            self.timerBetweenCommandSetup()
            errorcount = 0
            overallMotMatrix = [[0 for j in range(len(arrayMot))] for i in range(SAMPLENUMBER)]

            for time in range(0, SAMPLENUMBER):
                sleep(0.2)
                while True:
                    rawData = json.loads(self.c.new_data)
                    if STREAMTYPE2 in rawData:
                        rawData = rawData[STREAMTYPE2]
                        break
                index = 2
                for i in range(len(self.dataMot)):
                    self.dataMot[i] = rawData[index]
                    index += 1

                for i in range(len(self.dataMot)):
                    if (self.dataMot[i]) < 100000:
                        overallMotMatrix[time][i] = self.dataMot[i]
                    else:
                        if time == 0:
                            overallMotMatrix[time][i] = 0
                        else:
                            overallMotMatrix[time][i] = overallMotMatrix[time - 1][i]
                print(time + 1, " out of", SAMPLENUMBER)
            for z in range(SAMPLENUMBER):
                for i in range(len(self.dataMot)):
                    arrayMot[i] += round(overallMotMatrix[z][i], ROUNDING)

            for i in range(len(self.dataMot)):
                    arrayMot[i] = round(arrayMot[i] / SAMPLENUMBER, ROUNDING)

    # ====================== END SETTING UP API ========================
    def prettyPrintMatrix(self, matrix):
        for i in range(len(matrix)):
            print(matrix[i])

    def generateConfiguredArray(self, configuredInput, configuredLearntArray, learntArray):
        for i in range(len(self.data)):
            for j in range(len(self.data)):
                configuredInput[i][j] = round(self.data[i][j] / sum(learntArray[i]), ROUNDING)
                configuredLearntArray[i][j] = round(learntArray[i][j] / sum(self.data[i]), ROUNDING)

    def generateConfiguredArrayMot(self, configuredInputMot, configuredLearntArrayMot, learntArrayMot):
        for i in range(len(self.dataMot)):
            configuredInputMot[i] = round(self.dataMot[i] / sum(learntArrayMot), ROUNDING)
            configuredLearntArrayMot[i] = round(learntArrayMot[i] / sum(self.dataMot), ROUNDING)

    def compareInputWithCommands(self):
        # ===================== EEG POW INPUT PROCESS =====================
        configuredInput = [[0] * 5 for i in range(SENSORNUMBERS)]
        configuredForward = [[0] * 5 for i in range(SENSORNUMBERS)]
        configuredBackward = [[0] * 5 for i in range(SENSORNUMBERS)]
        configuredRight = [[0] * 5 for i in range(SENSORNUMBERS)]
        configuredLeft = [[0] * 5 for i in range(SENSORNUMBERS)]
        configuredUp = [[0] * 5 for i in range(SENSORNUMBERS)]
        configureddown = [[0] * 5 for i in range(SENSORNUMBERS)]

        self.generateConfiguredArray(configuredInput, configuredForward, self.forward)
        self.generateConfiguredArray(configuredInput, configuredBackward, self.backward)
        self.generateConfiguredArray(configuredInput, configuredRight, self.right)
        self.generateConfiguredArray(configuredInput, configuredLeft, self.left)
        self.generateConfiguredArray(configuredInput, configuredUp, self.up)
        self.generateConfiguredArray(configuredInput, configureddown, self.down)

        sensorValues = [0 for i in range(NUMBEROFCOMMANDS)]
        victoryPoints = [0 for i in range(NUMBEROFCOMMANDS)]
        # [0] = forward, [1] = backward, [2] = right, [3] = left, [4] = up, [5] = down
        for i in range(0, len(configuredInput)):
            for j in range(0, len(configuredInput)):
                if not self.isFac:
                    sensorValues[0] = abs(round(configuredForward[i][j]- configuredInput[i][j], ROUNDING))
                    sensorValues[1] = abs(round(configuredBackward[i][j]- configuredInput[i][j], ROUNDING))
                    sensorValues[2] = abs(round(configuredRight[i][j] - configuredInput[i][j], ROUNDING))
                    sensorValues[3] = abs(round(configuredLeft[i][j]- configuredInput[i][j], ROUNDING))
                    sensorValues[4] = abs(round(configuredUp[i][j] - configuredInput[i][j], ROUNDING))
                    sensorValues[5] = abs(round(configureddown[i][j] - configuredInput[i][j], ROUNDING))
                else:
                    if self.dataFac == self.switch1[0]:
                        sensorValues[0] = abs(round(configuredForward[i][j] - configuredInput[i][j], ROUNDING))
                        sensorValues[2] = abs(round(configuredRight[i][j] - configuredInput[i][j], ROUNDING))
                        sensorValues[3] = abs(round(configuredLeft[i][j] - configuredInput[i][j], ROUNDING))

                        sensorValues[1] = 100000
                        sensorValues[4] = 100000
                        sensorValues[5] = 100000
                    elif self.dataFac == self.switch2[0]:
                        sensorValues[1] = abs(round(configuredBackward[i][j] - configuredInput[i][j], ROUNDING))
                        sensorValues[4] = abs(round(configuredUp[i][j] - configuredInput[i][j], ROUNDING))
                        sensorValues[5] = abs(round(configureddown[i][j] - configuredInput[i][j], ROUNDING))

                        sensorValues[0] = 100000
                        sensorValues[2] = 100000
                        sensorValues[3] = 100000
                    else:
                        print("stay in position")
                        return 0

                """sensorValues[0] = abs(
                    round(self.forward[i][j] / sum(self.data[i]) - self.data[i][j] / sum(self.forward[i]), ROUNDING))
                sensorValues[1] = abs(
                    round(self.backward[i][j] / sum(self.data[i]) - self.data[i][j] / sum(self.backward[i]), ROUNDING))
                sensorValues[2] = abs(
                    round(self.right[i][j] / sum(self.data[i]) - self.data[i][j] / sum(self.right[i]), ROUNDING))
                sensorValues[3] = abs(
                    round(self.left[i][j] / sum(self.data[i]) - self.data[i][j] / sum(self.backward[i]), ROUNDING))"""

                for z in range(NUMBEROFCOMMANDS):
                    if sensorValues[z] == min(sensorValues):
                        victoryPoints[z] += 1

        if self.isMotActive:
            # ===================== MOT INPUT PROCESS =====================
            configuredInputMot = [0 for i in range(len(self.dataMot))]
            configuredForwardMot = [0 for i in range(len(self.dataMot))]
            configuredBackwardMot = [0 for i in range(len(self.dataMot))]
            configuredRightMot = [0 for i in range(len(self.dataMot))]
            configuredLeftMot = [0 for i in range(len(self.dataMot))]

            self.generateConfiguredArrayMot(configuredInputMot, configuredForwardMot, self.forwardMot)
            self.generateConfiguredArrayMot(configuredInputMot, configuredBackwardMot, self.backwardMot)
            self.generateConfiguredArrayMot(configuredInputMot, configuredRightMot, self.rightMot)
            self.generateConfiguredArrayMot(configuredInputMot, configuredLeftMot, self.leftMot)

            for i in range(len(configuredInputMot)):
                sensorValues[0] = abs(round(configuredForwardMot[i] - configuredForwardMot[i], ROUNDING))
                sensorValues[1] = abs(round(configuredBackwardMot[i] - configuredForwardMot[i], ROUNDING))
                sensorValues[2] = abs(round(configuredRightMot[i] - configuredForwardMot[i], ROUNDING))
                sensorValues[3] = abs(round(configuredLeftMot[i] - configuredForwardMot[i], ROUNDING))

                for z in range(NUMBEROFCOMMANDS):
                    if sensorValues[z] == min(sensorValues):
                        victoryPoints[z] += 2


        print(victoryPoints)
        print(self.dataFac)


        for i in range(NUMBEROFCOMMANDS):
            if victoryPoints[i] == max(victoryPoints):
                if i == 0:
                    print("Forward")
                elif i == 1:
                    print("Backward")
                elif i == 2:
                    print("Right")
                elif i == 3:
                    print("Left")
                elif i == 4:
                    print("Up")
                elif i == 5:
                    print("Down")

    def startHeadSet(self):
        enter = input("Press any key to continue...")
        while True:
            rawData = json.loads(self.c.new_data)
            if STREAMTYPE in rawData:
                rawData = rawData[STREAMTYPE]
                index = 0
                for i in range(0, len(self.data)):
                    for j in range(0, len(self.data)):
                        self.data[i][j] = rawData[index]
                        index += 1
                while True:
                    rawData = json.loads(self.c.new_data)
                    if STREAMTYPE2 in rawData:
                        rawData = rawData[STREAMTYPE2]
                        break
                if self.isMotActive:
                    for i in range(len(self.dataMot)):
                        self.dataMot[i] = rawData[i+2]
                if self.isFac:
                    self.dataFac = rawData[1]

                self.compareInputWithCommands()
                sleep(0.5)


#            self.showHeadsetSensorsWithText(self.data)


t = EmotivSingal()
profile_name = 'amibira'
t.setCommands()
t.hardcodedCommandValues()
t.startHeadSet()
