from threading import Thread
import tkinter as tk
import json
from time import sleep
from DroneControl.MovementEnum import movement
from EmotivInterfaceHandle.cortex import Cortex
import json

# THESE ARE PREDEFINED STATE THAT WORKS THE BEST DO NOT CHANGE IT!
STREAMTYPE = 'pow'
STREAMTYPE2 = 'fac'
SAMPLENUMBER = 50
SINGALMISTAKES = 5
SENSORNUMBERS = 5
NUMBEROFCOMMANDS = 6
WAITINGTIME = 5
ROUNDING = 10
MAXIMUMDIFFERENCE = 15
DIFFERENCEEPSILON = 3
MINIMUMVICTORYPOINTS = 10
MAXIMUMDIFFERENCEOFTWOPOINTS = 1


class EmotivSingalForButtons:
    def __init__(self, textArea, isMentalCommandOn, control):
        self.textArea = textArea
        self.isMentalCommandOn = isMentalCommandOn
        self.control = control
        self.testArray = []

        # COMMANDS
        self.isMotActive = False
        self.isFac = False
        self.isRecording = False

        # EEG POW
        self.forward = [[1000] * 5 for i in range(SENSORNUMBERS)]
        self.backward = [[1000] * 5 for i in range(SENSORNUMBERS)]
        self.right = [[1000] * 5 for i in range(SENSORNUMBERS)]
        self.left = [[1000] * 5 for i in range(SENSORNUMBERS)]
        self.up = [[1000] * 5 for i in range(SENSORNUMBERS)]
        self.down = [[1000] * 5 for i in range(SENSORNUMBERS)]

        # FACIAL
        if STREAMTYPE2 == 'fac':
            self.isFac = True

        self.switch1 = ["", self.forward, self.right, self.left]
        self.switch2 = ["", self.backward, self.up, self.down]

        # MOTION
        if STREAMTYPE2 == 'mot':
            self.isMotActive = True

        self.forwardMot = [0 for i in range(10)]
        self.backwardMot = [0 for i in range(10)]
        self.rightMot = [0 for i in range(10)]
        self.leftMot = [0 for i in range(10)]
        self.upMot = [0 for i in range(10)]
        self.downMot = [0 for i in range(10)]

        self.commandsArrayRef = [self.forward, self.backward, self.right, self.left]  # down up

        self.data = [[0] * 5 for i in range(SENSORNUMBERS)]
        self.dataMot = [0 for i in range(10)]
        self.dataFac = "None"

        self.user = {
            "license": "",
            "client_id": "FNQTigpFd9ftHssVjFUcpfPhftV2zAk2S1WFCmEL",
            "client_secret": "P2S7QxmjK4Smiq3cHiHCh6koBO7I6B1aRk4S9PbUa4hFf4hUy9hSF3gi30RYvgzmePzbIluxQGJqqavz8nQsMBUnJW6gNbshBWyilkVhirL9MAf1NoKk3bAWqVpT3uUp",
            "debit": 100
        }
        self.c = Cortex(self.user, debug_mode=True)
        self.c.do_prepare_steps()

    def setSwitches(self):
        self.switch1[0] = 'neutral'
        self.switch2[0] = 'surprise'

    def mirrorCommandsToSwitch(self):
        self.backward = self.forward
        self.up = self.right
        self.down = self.left

    def hardcodedCommandValues(self):
        # SWITCH 1 IF USED
        self.forward = [
            [4.97974, 6.70124, 3.26342, 5.5415, 3.6903]
            , [1.78114, 3.4149, 5.95846, 14.34814, 10.8744]
            , [5.31774, 9.17424, 1.82484, 2.17316, 1.54132]
            , [0.75424, 0.82396, 1.04422, 0.76748, 0.91452]
            , [6.11462, 7.57658, 3.45446, 6.55876, 4.09448]]
        self.right = [
            [7.64808, 7.87766, 6.75988, 12.72396, 8.99724]
            , [4.5144, 6.37122, 7.70604, 16.17534, 9.43288]
            , [4.21588, 6.97484, 4.87366, 16.17892, 18.41662]
            , [9.06302, 9.05384, 9.91356, 14.36226, 11.75874]
            , [7.3032, 9.1657, 7.6972, 12.89378, 9.55336]
        ]
        self.left = [
            [7.14388, 4.79984, 2.07846, 0.63794, 0.87464]
            , [8.94588, 3.74622, 2.14394, 1.6819, 1.9361]
            , [3.91592, 3.21316, 1.06688, 0.50398, 0.67152]
            , [2.01048, 2.4005, 1.50172, 0.94768, 0.80392]
            , [9.74688, 4.97496, 1.73292, 0.50926, 0.75536]]


    def saveCustomPreset(self, filename):
        try:
            if filename == "":
                raise FileNotFoundError
            print(filename[-5:len(filename)])
            if filename[-5:len(filename)] != ".json":
                filename += ".json"
            file = open(filename, "w")
            file.write(json.dumps(
                {"forward": self.forward, "left": self.left, "right": self.right, "backward": self.backward,
                 "down": self.down, "up": self.up}))
            file.close()
            self.textArea.insert(tk.END, "Custom Preset Saved\n")
            self.textArea.see(tk.END)
        except FileNotFoundError:
            self.textArea.insert(tk.END, "Error during save\n")
            self.textArea.see(tk.END)
        except UnicodeDecodeError:
            self.textArea.insert(tk.END, "Error during save\n")
            self.textArea.see(tk.END)
        except json.decoder.JSONDecodeError:
            self.textArea.insert(tk.END, "Error during save\n")
            self.textArea.see(tk.END)


    def loadCustomPreset(self, filename):
        try:
            file = open(filename, "r")
            savedCommands = json.loads(file.readline())
            self.forward = savedCommands["forward"]
            self.left = savedCommands["left"]
            self.right = savedCommands["right"]
            self.backward = savedCommands["backward"]
            self.down = savedCommands["down"]
            self.up = savedCommands["up"]
            file.close()
            self.textArea.insert(tk.END, "Previous Custom Preset Loaded\n")
            self.textArea.see(tk.END)
        except TypeError:
            self.textArea.insert(tk.END, "The file has a invalid format\n")
            self.textArea.see(tk.END)
        except KeyError:
            self.textArea.insert(tk.END, "The file has a invalid format\n")
            self.textArea.see(tk.END)
        except FileNotFoundError:
            self.textArea.insert(tk.END, "The file was not found\n")
            self.textArea.see(tk.END)
        except UnicodeDecodeError:
            self.textArea.insert(tk.END, "The file is not a JSON\n")
            self.textArea.see(tk.END)
        except json.decoder.JSONDecodeError:
            self.textArea.insert(tk.END, "The file is not a JSON\n")
            self.textArea.see(tk.END)

    def setUpEmotiv(self):
        stream = [STREAMTYPE, STREAMTYPE2]
        self.c.query_profile()
        self.c.setup_profile('amibira', 'load')
        Thread(target=self.c.sub_request, args=(stream,), daemon=True).start()
        sleep(0.5)

    def showHeadsetSensorsWithText(self, data):
        print("    AF3: ", round(sum(data[0], 0)), "          AF4: ", round(sum(data[4]), 0))
        print("                          ")
        print("T7: ", round(sum(data[1], 0)), "                        T8: ", round(sum(data[3]), 0))
        print("                          ")
        print("            Pz: ", round(sum(data[2]), 0), "          ")

    # ====================== START SETTING UP API ========================
    def setCommands(self):
        stream = [STREAMTYPE, STREAMTYPE2]
        self.c.query_profile()
        self.c.setup_profile('amibira', 'load')
        Thread(target=self.c.sub_request, args=(stream,), daemon=True).start()
        sleep(1)

        print("Setting the swicthes first")
        self.setSwitches()
        # switch1
        print("Forward")
        self.timerBetweenCommandSetup()
        self.setCommandValue(self.forward, self.forwardMot)

        print("Right")
        self.timerBetweenCommandSetup()
        self.setCommandValue(self.right, self.rightMot)

        print("Left")
        self.timerBetweenCommandSetup()
        self.setCommandValue(self.left, self.leftMot)
        # switch2
        print("Backward")
        self.timerBetweenCommandSetup()
        self.setCommandValue(self.backward, self.backwardMot)

        print("Up")
        self.timerBetweenCommandSetup()
        self.setCommandValue(self.up, self.upMot)

        print("Down")
        self.timerBetweenCommandSetup()
        self.setCommandValue(self.down, self.downMot)

        print("Forward")
        self.prettyPrintMatrix(self.forward)
        print("right")
        self.prettyPrintMatrix(self.right)
        print("left")
        self.prettyPrintMatrix(self.left)

        if STREAMTYPE2 == "mot":
            print("Forward")
            print(self.forwardMot)
            print("backward")
            print(self.backwardMot)
            print("right")
            print(self.rightMot)
            print("left")
            print(self.leftMot)

    #        print("up")
    #        self.prettyPrintMatrix(self.up)
    #        print("down")
    #        self.prettyPrintMatrix(self.down)

    def timerBetweenCommandSetup(self):
        for i in range(WAITINGTIME):
            self.textArea.insert(tk.END, "Next Command in : " + str(5 - i) + " sec\n")
            self.textArea.see(tk.END)
            sleep(1)

    def setCommandValue(self, array, arrayMot, buttons):
        try:
            for button in buttons:
                button['state'] = 'disable'

            self.timerBetweenCommandSetup()
            errorcount = 0
            overallMatrix = [[[0 for z in range(len(array))] for j in range(len(array))] for i in
                             range(SAMPLENUMBER)]

            # Reset command matrix to 0
            for i in range(len(array)):
                for j in range(len(array)):
                    array[i][j] = 0

            # ================= EEG POWER DATA COLLECTION ===========================
            for time in range(0, SAMPLENUMBER):
                while True:
                    rawData = json.loads(self.c.new_data)
                    if STREAMTYPE in rawData:
                        rawData = rawData[STREAMTYPE]
                        break
                index = 0
                for i in range(0, len(self.data)):
                    for j in range(0, len(self.data)):
                        self.data[i][j] = rawData[index]
                        index += 1

                for i in range(0, len(array)):
                    for j in range(0, len(array)):
                        if (self.data[i][j]) < 100:
                            overallMatrix[time][i][j] = self.data[i][j]
                        else:
                            if time == 0:
                                overallMatrix[time][i][j] = 0
                            else:
                                overallMatrix[time][i][j] = overallMatrix[time - 1][i][j]
                self.textArea.insert(tk.END, str(time + 1) + " out of " + str(SAMPLENUMBER) + "\n")
                self.textArea.see(tk.END)
            for z in range(SAMPLENUMBER):
                for i in range(len(self.data)):
                    for j in range(len(self.data)):
                        array[i][j] += round(overallMatrix[z][i][j], ROUNDING)

            for i in range(len(self.data)):
                for j in range(len(self.data)):
                    array[i][j] = round(array[i][j] / SAMPLENUMBER, ROUNDING)
            if self.isMotActive:
                # ================= MOTION DATA COLLECTION ===========================
                print("Collecting motion data....")
                self.textArea.insert(tk.END, "Collecting motion data....\n")
                self.textArea.see(tk.END)
                self.timerBetweenCommandSetup()
                overallMotMatrix = [[0 for j in range(len(arrayMot))] for i in range(SAMPLENUMBER)]

                for time in range(0, SAMPLENUMBER):
                    sleep(0.2)
                    while True:
                        rawData = json.loads(self.c.new_data)
                        if STREAMTYPE2 in rawData:
                            rawData = rawData[STREAMTYPE2]
                            break
                    index = 2
                    for i in range(len(self.dataMot)):
                        self.dataMot[i] = rawData[index]
                        index += 1

                    for i in range(len(self.dataMot)):
                        if (self.dataMot[i]) < 100000:
                            overallMotMatrix[time][i] = self.dataMot[i]
                        else:
                            if time == 0:
                                overallMotMatrix[time][i] = 0
                            else:
                                overallMotMatrix[time][i] = overallMotMatrix[time - 1][i]
                    self.textArea.insert(tk.END, str(time + 1) + " out of " + str(SAMPLENUMBER) + "\n")
                    self.textArea.see(tk.END)
                for z in range(SAMPLENUMBER):
                    for i in range(len(self.dataMot)):
                        arrayMot[i] += round(overallMotMatrix[z][i], ROUNDING)

                for i in range(len(self.dataMot)):
                    arrayMot[i] = round(arrayMot[i] / SAMPLENUMBER, ROUNDING)

            self.textArea.insert(tk.END, "END OF COMMAND\n\n")
            self.textArea.see(tk.END)

            # Clear self.data
            self.data = [[0] * 5 for i in range(SENSORNUMBERS)]
        finally:
            for button in buttons:
                button['state'] = 'active'

    # ====================== END SETTING UP API ========================
    def prettyPrintMatrix(self, matrix):
        for i in range(len(matrix)):
            print(matrix[i])

    def compareInputWithCommands(self):
        # ===================== EEG POW INPUT PROCESS =====================

        configuredInput = self.data
        configuredForward = self.forward
        configuredBackward = self.backward
        configuredRight = self.right
        configuredLeft = self.left
        configuredUp = self.up
        configureddown = self.down

        sensorValues = [0 for i in range(NUMBEROFCOMMANDS)]
        victoryPoints = [0 for i in range(NUMBEROFCOMMANDS)]
        # [0] = forward, [1] = backward, [2] = right, [3] = left, [4] = up, [5] = down
        for i in range(0, len(configuredInput)):
            for j in range(0, len(configuredInput)):
                if not self.isFac:
                    sensorValues[0] = abs(round(configuredForward[i][j] - configuredInput[i][j], ROUNDING))
                    sensorValues[1] = abs(round(configuredBackward[i][j] - configuredInput[i][j], ROUNDING))
                    sensorValues[2] = abs(round(configuredRight[i][j] - configuredInput[i][j], ROUNDING))
                    sensorValues[3] = abs(round(configuredLeft[i][j] - configuredInput[i][j], ROUNDING))
                    sensorValues[4] = abs(round(configuredUp[i][j] - configuredInput[i][j], ROUNDING))
                    sensorValues[5] = abs(round(configureddown[i][j] - configuredInput[i][j], ROUNDING))
                else:
                    if self.dataFac == self.switch1[0]:
                        sensorValues[0] = abs(round(configuredForward[i][j] - configuredInput[i][j], ROUNDING))
                        sensorValues[2] = abs(round(configuredRight[i][j] - configuredInput[i][j], ROUNDING))
                        sensorValues[3] = abs(round(configuredLeft[i][j] - configuredInput[i][j], ROUNDING))

                        sensorValues[1] = 100000
                        sensorValues[4] = 100000
                        sensorValues[5] = 100000
                    elif self.dataFac == self.switch2[0] or self.dataFac == "Frown":
                        sensorValues[1] = abs(round(configuredBackward[i][j] - configuredInput[i][j], ROUNDING))
                        sensorValues[4] = abs(round(configuredUp[i][j] - configuredInput[i][j], ROUNDING))
                        sensorValues[5] = abs(round(configureddown[i][j] - configuredInput[i][j], ROUNDING))

                        sensorValues[0] = 100000
                        sensorValues[2] = 100000
                        sensorValues[3] = 100000
                    else:
                        return -1

                for z in range(NUMBEROFCOMMANDS):
                    if abs(sensorValues[z] - min(sensorValues)) <= MAXIMUMDIFFERENCEOFTWOPOINTS and min(
                            sensorValues) <= DIFFERENCEEPSILON:
                        victoryPoints[z] += 1

        if self.isMotActive:
            # ===================== MOT INPUT PROCESS =====================
            configuredInputMot = [0 for i in range(len(self.dataMot))]
            configuredForwardMot = [0 for i in range(len(self.dataMot))]
            configuredBackwardMot = [0 for i in range(len(self.dataMot))]
            configuredRightMot = [0 for i in range(len(self.dataMot))]
            configuredLeftMot = [0 for i in range(len(self.dataMot))]

            for i in range(len(configuredInputMot)):
                sensorValues[0] = abs(round(configuredForwardMot[i] - configuredForwardMot[i], ROUNDING))
                sensorValues[1] = abs(round(configuredBackwardMot[i] - configuredForwardMot[i], ROUNDING))
                sensorValues[2] = abs(round(configuredRightMot[i] - configuredForwardMot[i], ROUNDING))
                sensorValues[3] = abs(round(configuredLeftMot[i] - configuredForwardMot[i], ROUNDING))

                for z in range(NUMBEROFCOMMANDS):
                    if sensorValues[z] == min(sensorValues):
                        victoryPoints[z] += 2

        if max(victoryPoints) > MINIMUMVICTORYPOINTS:
            for i in range(NUMBEROFCOMMANDS):
                if victoryPoints[i] == max(victoryPoints):
                    if i == 0:
                        # print("Forward")
                        self.control[0] = movement.FORWARD
                        self.textArea.insert(tk.END, "Forward: " + str(victoryPoints) + "\n")
                        self.textArea.see(tk.END)
                        break
                    elif i == 1:
                        # print("Backward")
                        self.control[0] = movement.BACK
                        self.textArea.insert(tk.END, "Backward: " + str(victoryPoints) + "\n")
                        self.textArea.see(tk.END)
                        break
                    elif i == 2:
                        # print("Right")
                        self.control[0] = movement.ROTATE_RIGHT
                        self.textArea.insert(tk.END, "Right: " + str(victoryPoints) + "\n")
                        self.textArea.see(tk.END)
                        break
                    elif i == 3:
                        # print("Left")
                        self.control[0] = movement.ROTATE_LEFT
                        self.textArea.insert(tk.END, "Left: " + str(victoryPoints) + "\n")
                        self.textArea.see(tk.END)
                        break
                    elif i == 4:
                        # print("Up")
                        self.control[0] = movement.UP
                        self.textArea.insert(tk.END, "Up: " + str(victoryPoints) + "\n")
                        self.textArea.see(tk.END)
                        break
                    elif i == 5:
                        # print("Down")
                        self.control[0] = movement.DOWN
                        self.textArea.insert(tk.END, "Down: " + str(victoryPoints) + "\n")
                        self.textArea.see(tk.END)
                        break


    def saveRecording(self, filename, button):
        try:
            self.isRecording = True
            self.recFile = filename
            button['state'] = 'disabled'
            print(filename[-5:len(filename)])
            if filename[-5:len(filename)] != ".json":
                filename += ".json"
            self.recFile = open(filename, "w")
            self.recFile.write(json.dumps(
                {"forward": self.forward, "left": self.left, "right": self.right, "backward": self.backward,
                 "down": self.down, "up": self.up}))
            self.recFile.write("\n")
        except FileNotFoundError:
            self.textArea.insert(tk.END, "Error during save\n")
            self.textArea.see(tk.END)
        except UnicodeDecodeError:
            self.textArea.insert(tk.END, "Error during save\n")
            self.textArea.see(tk.END)
        except IndexError:
            self.textArea.insert(tk.END, "Error during save\n")
            self.textArea.see(tk.END)
        finally:
            button['state'] = 'active'

    def replayRecord(self, filename, button, index, loadedRecords, isReplayOn):
        file = None
        try:
            if button is not None:
                button['state'] = 'disable'
            self.isMentalCommandOn[0] = True
            file = open(filename, "r")
            savedCommands = json.loads(file.readline())
            self.forward = savedCommands["forward"]
            self.left = savedCommands["left"]
            self.right = savedCommands["right"]
            self.backward = savedCommands["backward"]
            self.down = savedCommands["down"]
            self.up = savedCommands["up"]
            self.textArea.insert(tk.END, "RECORDING IS BEING IN REPLAY\n")
            self.textArea.see(tk.END)
            #NEW TYPE

            savedCommands = file.readline()
            while savedCommands:
                savedCommands = json.loads(savedCommands)
                loadedRecords.append([savedCommands["current"], savedCommands["dataFac"]])
                savedCommands = file.readline()

            while self.isMentalCommandOn[0]:
                if isReplayOn[0]:
                    self.data = loadedRecords[index[0]][0]
                    self.dataFac = loadedRecords[index[0]][1]
                    self.compareInputWithCommands()

                    self.testArray.append(self.control[0])
                    index[0] += 1
                    if index[0] >= len(loadedRecords)-1:
                        index[0] = 0
                        isReplayOn[0] = False

                    if button is not None:
                        sleep(0.5)
                else:
                    self.control[0] = movement.STAY
                    sleep(0.5)


            #WORKING ALREADY OLD TYPE
            """
            while self.isMentalCommandOn[0]:
                data = file.readline()
                if not data:
                    break
                data = json.loads(data)
                self.data = data["current"]
                self.dataFac = data["dataFac"]
                self.compareInputWithCommands()
                self.testArray.append(self.control[0])
                if button is not None:
                    sleep(0.5)
            """
            file.close()


        except TypeError:
            self.textArea.insert(tk.END, "The file has a invalid format\n")
            self.textArea.see(tk.END)
        except FileNotFoundError:
            self.textArea.insert(tk.END, "The file was not found\n")
            self.textArea.see(tk.END)
        except AttributeError:
            self.textArea.insert(tk.END, "Chose a file!\n")
            self.textArea.see(tk.END)
        except UnicodeDecodeError:
            self.textArea.insert(tk.END, "The file is not a JSON\n")
            self.textArea.see(tk.END)
        except json.decoder.JSONDecodeError:
            self.textArea.insert(tk.END, "The file is not a JSON\n")
            self.textArea.see(tk.END)
        finally:
            if file is not None:
                file.close()
            if button is not None:
                button['state'] = 'active'
            self.isMentalCommandOn[0] = False
            self.textArea.insert(tk.END, "REPLAY HAS BEEN STOPPED\n")
            self.textArea.see(tk.END)
            self.control[0] = movement.STAY

    def startHeadSet(self, button):
        try:
            button['state'] = 'disable'
            while self.isMentalCommandOn[0]:
                rawData = json.loads(self.c.new_data)
                if STREAMTYPE in rawData:
                    rawData = rawData[STREAMTYPE]
                    index = 0
                    for i in range(0, len(self.data)):
                        for j in range(0, len(self.data)):
                            self.data[i][j] = rawData[index]
                            index += 1
                    while True:
                        rawData = json.loads(self.c.new_data)
                        if STREAMTYPE2 in rawData:
                            rawData = rawData[STREAMTYPE2]
                            break
                    if self.isMotActive:
                        for i in range(len(self.dataMot)):
                            self.dataMot[i] = rawData[i + 2]
                    if self.isFac:
                        self.dataFac = rawData[1]

                    if self.isRecording:
                        self.recFile.write(json.dumps({"current": self.data, "dataFac": self.dataFac}))
                        self.recFile.write("\n")

                    self.compareInputWithCommands()
                    sleep(0.5)

        finally:
            if button is not None:
                button['state'] = 'active'
            self.control[0] = movement.STAY
            if self.isRecording:
                self.isRecording = False
                self.recFile.close()
                self.textArea.insert(tk.END, "Recording Has Stopped\n")
                self.textArea.see(tk.END)
